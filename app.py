from flask import Flask, url_for
from flask.ext.classy import FlaskView

app = Flask(__name__)

#Trivial class to be instantiated when the app is.
class Linkify(FlaskView):
	def index(self):
		return "Let's Get <a href='/linkify/linked'>Linked</a>"

#respond to GET request with parameters.
	def get(self, text):
		return "<a href='http://beta.aerian.com'><h1>%s</h1></a>" % (text)

#link Flask app with class
Linkify.register(app, route_base='/')

if __name__ == "__main__":
	app.run()

#to extend the class

#class LinkifySub(Linkify):
#	pass

#The above could be extended to provide a link in a POST request to modify the destination
